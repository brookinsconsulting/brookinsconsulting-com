<?php /* #?ini charset="utf-8"?

[DebugSettings]
DebugOutput=disabled

[TemplateSettings]
ShowUsedTemplates=enabled

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=ezecosystem
ActiveExtensions[]=all2egooglesitemaps
ActiveExtensions[]=ezsh
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=eztags
ActiveExtensions[]=ezautosave
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezwebin
ActiveExtensions[]=ezie
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezprestapiprovider

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=ezwebin_site_user
SiteList[]
SiteList[]=ezwebin_site_user
SiteList[]=ezwebin_site_admin
SiteList[]=eze_user_local
SiteList[]=eze_user_admin
SiteList[]=eng
RootNodeDepth=1
SiteName=eZ Ecosystem
MetaDataArray[author]=eZ Ecosystem
MetaDataArray[copyright]=eZ Ecosystem
MetaDataArray[description]=eZ Publish Community Ecosystem Planet
MetaDataArray[keywords]=ezpublish, eZ Publish, eZ, blogs, bloggers, planet, tag, community, ecosystem, developer, blog, cms, publish, e-commerce, content management, development framework
SiteURL=ezecosystem/

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=ezwebin_site_user
AvailableSiteAccessList[]=ezwebin_site_admin
AvailableSiteAccessList[]=eze_user_local
AvailableSiteAccessList[]=eze_admin_local
AvailableSiteAccessList[]=eng
MatchOrder=host
HostMatchMapItems[]
HostMatchMapItems[]=ezecosystem.org;ezwebin_site_user
HostMatchMapItems[]=www.ezecosystem.org;ezwebin_site_user
HostMatchMapItems[]=admin.ezecosystem.org;ezwebin_site_admin
HostMatchMapItems[]=ezecosystem.com;ezwebin_site_user
HostMatchMapItems[]=www.ezecosystem.com;ezwebin_site_user
HostMatchMapItems[]=admin.ezecosystem.com;ezwebin_site_admin
HostMatchMapItems[]=ezecosystem;eze_user_local
HostMatchMapItems[]=admin.ezecosystem;eze_admin_local
ForceVirtualHost=true

[DesignSettings]
DesignLocationCache=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[eng]=Eng

[FileSettings]
VarDir=var/ezwebin_site

[MailSettings]
Transport=sendmail
AdminEmail=info@brookinsconsulting.com
EmailSender=info@ezecosystem.org

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline
*/ ?>