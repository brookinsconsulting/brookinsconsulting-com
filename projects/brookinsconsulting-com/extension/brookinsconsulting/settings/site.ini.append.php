<?php /*

# [TemplateSettings]
# ExtensionAutoloadPath[]=brookinsconsulting

[RegionalSettings]
TranslationExtensions[]=brookinsconsulting

[RSSSettings]
DefaultFeedItemClasses[folder]=article

[RSSSettings_article]
FeedObjectAttributeMap[title]=title
FeedObjectAttributeMap[description]=intro
FeedObjectAttributeMap[category]=tags

*/ ?>
